import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'app/fancy-box/item';

@Component({
  selector: 'app-an-item',
  template: `
    <span>
      ({{ item.id }}) {{ item.name }}
    </span>
  `,
  styles: []
})
export class AnItemComponent implements OnInit {
  @Input()
  item: Item;

  constructor() {}

  ngOnInit() {}
}
