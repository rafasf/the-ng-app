import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { AnItemComponent } from 'app/fancy-box/an-item/an-item.component';
import { By } from '@angular/platform-browser';

describe('An Item', () => {
  let comp: AnItemComponent;
  let fixture: ComponentFixture<AnItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnItemComponent);
    comp = fixture.componentInstance;
  });

  it('displays item name', () => {
    comp.item = { id: 'abc', name: 'the first' };
    fixture.detectChanges();

    const anItem = fixture.debugElement.query(By.css('span'));

    expect(anItem.nativeElement.textContent).toContain('(abc) the first');
  });
});
