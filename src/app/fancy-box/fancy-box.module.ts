import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from 'app/fancy-box/fancy-box.reducer';
import { FancyBoxEffects } from 'app/fancy-box/fancy-box.effects';
import { AnItemComponent } from 'app/fancy-box/an-item/an-item.component';
import { ItemListComponent } from 'app/fancy-box/item-list/item-list.component';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('fancyBox', reducer),
    EffectsModule.forFeature([FancyBoxEffects])
  ],
  declarations: [ItemListComponent, AnItemComponent],
  exports: [ItemListComponent]
})
export class FancyBoxModule {}
