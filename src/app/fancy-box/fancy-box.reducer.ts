import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { Item } from 'app/fancy-box/item';
import {
  FancyBoxActions,
  FancyBoxActionTypes
} from 'app/fancy-box/fancy-box.actions';

export const featureAdapter: EntityAdapter<Item> = createEntityAdapter<Item>({
  selectId: e => e.id
});

export interface State extends EntityState<Item> {
  isLoading?: boolean;
  error?: any;
}

export const initialState: State = featureAdapter.getInitialState({
  isLoading: false,
  error: null
});

export function reducer(state = initialState, action: FancyBoxActions): State {
  switch (action.type) {
    case FancyBoxActionTypes.LoadFancyBoxs:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case FancyBoxActionTypes.LoadSuccess:
      return featureAdapter.addAll(action.payload.items, {
        ...state,
        isLoading: false,
        error: null
      });
    default:
      return state;
  }
}

const fancyBoxState = createFeatureSelector<State>('fancyBox');
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = featureAdapter.getSelectors(fancyBoxState);
