import { Action } from '@ngrx/store';
import { Item } from 'app/fancy-box/item';

export enum FancyBoxActionTypes {
  LoadFancyBoxs = '[FancyBox] Load FancyBoxs',
  LoadSuccess = '[FancyBox] Loaded'
}

export class LoadFancyBox implements Action {
  readonly type = FancyBoxActionTypes.LoadFancyBoxs;
}

export class LoadSuccess implements Action {
  readonly type = FancyBoxActionTypes.LoadSuccess;

  constructor(public payload: { items: Item[] }) {}
}

export type FancyBoxActions = LoadFancyBox | LoadSuccess;
