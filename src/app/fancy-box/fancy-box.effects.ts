import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Item } from 'app/fancy-box/item';
import {
  FancyBoxActionTypes,
  LoadSuccess
} from 'app/fancy-box/fancy-box.actions';

@Injectable()
export class FancyBoxEffects {
  myItems: Item[] = [new Item('one', 'banana'), new Item('two', 'apple')];

  @Effect()
  effect$ = this.actions$.pipe(
    ofType(FancyBoxActionTypes.LoadFancyBoxs),
    switchMap(action => of(new LoadSuccess({ items: this.myItems })))
  );

  constructor(private actions$: Actions) {}
}
