import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Item } from 'app/fancy-box/item';
import { LoadFancyBox } from 'app/fancy-box/fancy-box.actions';
import * as fromFancyBox from 'app/fancy-box/fancy-box.reducer';

@Component({
  selector: 'app-item-list',
  template: `
    <ul>
      <li *ngFor="let i of items$ | async">
        <app-an-item [item]="i"></app-an-item>
      </li>
    </ul>
  `,
  styles: []
})
export class ItemListComponent implements OnInit {
  items$: Observable<Item[]>;

  constructor(private store: Store<fromFancyBox.State>) {}

  ngOnInit() {
    this.items$ = this.store.select(fromFancyBox.selectAll);
    this.store.dispatch(new LoadFancyBox());
  }
}
