import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';

import * as fromFancyBox from '../fancy-box.reducer';

import { ItemListComponent } from './item-list.component';
import { AnItemComponent } from '../an-item/an-item.component';
import { LoadFancyBox, LoadSuccess } from '../fancy-box.actions';

describe('Item List', () => {
  let comp: ItemListComponent;
  let fixture: ComponentFixture<ItemListComponent>;
  let store: Store<fromFancyBox.State>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          fancyBox: fromFancyBox.reducer
        })
      ],
      declarations: [ItemListComponent, AnItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(ItemListComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Creation', () => {
    it('requests all items to be loaded', () => {
      expect(store.dispatch).toHaveBeenCalledWith(new LoadFancyBox());
    });

    it('loads all items', () => {
      const someItems = [
        { id: '1', name: 'banana' },
        { id: 'a', name: 'orange' }
      ];

      store.dispatch(new LoadSuccess({ items: someItems }));
      fixture.detectChanges();

      comp.items$.subscribe(allItems => {
        expect(allItems).toEqual([
          { id: '1', name: 'banana' },
          { id: 'a', name: 'orange' }
        ]);
      });
    });
  });
});
