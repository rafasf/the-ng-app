import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { reducer } from 'app/fancy-box/fancy-box.reducer';

export interface State {}

export const reducers: ActionReducerMap<State> = {
  fancyBox: reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
