import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StoreModule, ActionReducerMap } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from 'src/app/app.component';
import { FancyBoxModule } from 'app/fancy-box/fancy-box.module';
import { reducers } from 'app/reducers';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FancyBoxModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    }),
    EffectsModule.forRoot([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
